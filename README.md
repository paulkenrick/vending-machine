# Vending Machine App

## How to run the app

From the root directory simply run the following console command: `ruby lib/vending_machine.rb`.

## How to use the app

The main menu presents a list of products from which the user may choose.  Simply follow the prompts to purchase.

If you are a technician you may insert your key at this point (press 'K') to view the technician's options.  Again simply select the option required and follow the prompts to complete a task.

## Construction

The vending machine is comprised of a number of different classes.  These are briefly described below:

**UserInterface Class**

The UserInterface class is responsible for all the interactions with the user.  It processes user input and makes appropriate calls to the other classes (ProductHolder, CoinHolder, etc)

**ProductHolder Class**

The ProductHolder class is responsible for storing Products.  It can report to the user interface on stock levels and can load or unload products into its inventory.

**Product Class**

Each instance of this class represents a different type of product, and an amount held.

**CoinHolder Class**

The CoinHolder class is responsible for storing Coins.  It can report to the user interface on coin levels and can load or unload coins into its inventory of coins.

**Coin Class**

Each instance of this class represents a different denomination of coin, and an amount held.

**CoinConverter Class**

The CoinConverter class provides methods to assist the user interface to convert coins to gbp (when accepting coins from the user), or gbp to coins (when providing change).  It also holds sole knowledge of valid coin denominations, which means that swapping it out for say, a USCoinCoverter class, would allow the whole vending machine to function in the US.

## Testing

I have included unit tests for each method.  Please run these with `rspec spec`.
