class CoinConverter

  COINS_ACCEPTED = %w(1p 2p 5p 10p 20p 50p £1 £2)

  def self.coins_accepted
    COINS_ACCEPTED
  end

  def self.valid_denomination?(denomination)
    return true if CoinConverter::COINS_ACCEPTED.include?(denomination)
    false
  end

  def self.convert_coins_to_gbp(coins_hash)
    total = 0.0
    coins_hash.keys.each do |key|
      case key
      when '1p'
        total += (0.01 * coins_hash[key])
      when '2p'
        total += (0.02 * coins_hash[key])
      when '5p'
        total += (0.05 * coins_hash[key])
      when '10p'
        total += (0.10 * coins_hash[key])
      when '20p'
        total += (0.20 * coins_hash[key])
      when '50p'
        total += (0.50 * coins_hash[key])
      when '£1'
        total += (1.00 * coins_hash[key])
      when '£2'
        total += (2.00 * coins_hash[key])
      end
    end
    total
  end

  def self.convert_gbp_to_coins(gbp_amount, coin_holder)
    coins_hash = {}

    coin_holder.retrieve_available_coins.reverse.each do |coin|
      denomination_value =  self.convert_coins_to_gbp({ coin.denomination => 1 })

      remaining_change_amount = (gbp_amount - self.convert_coins_to_gbp(coins_hash)).round(2)

      if remaining_change_amount / denomination_value >= 1
        coins_hash[coin.denomination] = [coin.number, (remaining_change_amount / denomination_value).round(2).to_i].min
      end
    end

    return nil if self.convert_coins_to_gbp(coins_hash).round(2) < gbp_amount.round(2)
    coins_hash
  end
end
