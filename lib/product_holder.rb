require_relative 'product'

class ProductHolder

  attr_reader :capacity
  attr_reader :products

  DEFAULT_CAPACITY = 40

  def initialize(capacity = DEFAULT_CAPACITY)
    @capacity = capacity
    @products = []
  end

  def load_product(name, price, amount)
    if existing_product(name, price)
      existing_product(name, price).amount += amount
    else
      @products << Product.new(name, price, amount)
    end
  end

  def unload_product(name, price, amount)
    if product = existing_product(name, price)
      product.amount = product.amount - amount
      if product.amount == 0
        @products.delete(product)
        return true
      end
    end
    false
  end

  def existing_product(name, price)
    existing_products = @products.select{ |product| product.name == name && product.price == price }
    if existing_products.any?
      return existing_products.first
    end
  end

  def check_stock
    stock_result = "Vending machine currently holds the following products:\n\n"
    @products.each do |product|
      unit_string = (product.amount > 1 ? "units" : "unit")
      stock_result += "#{product.name} - #{product.amount} #{unit_string}\n"
    end
    stock_result + "\n"
  end

  def retrieve_available_products
    @products
  end

  def dispense(product)
    product.amount -= 1
    @products.delete(product) if product.amount == 0
  end
end
