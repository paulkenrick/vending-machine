require_relative 'coin'

class CoinHolder

  attr_reader :coins

  def initialize
    @coins = []
  end

  def load_coin(denomination, number=1)
    new_coin = existing_coin(denomination)
    if new_coin
      new_coin.number += number
    else
      insertion_index = -1
      @coins.each_with_index do |coin, index|
        if CoinConverter.convert_coins_to_gbp({ coin.denomination => 1 }) > CoinConverter.convert_coins_to_gbp({ denomination => 1 })
          insertion_index = index
          break
        end
      end
      @coins.insert(insertion_index, Coin.new(denomination, number))
    end
  end

  def unload_coin(denomination, number)
    if new_coin = existing_coin(denomination)
      new_coin.number -= number
      @coins.delete(new_coin) if new_coin.number == 0
      return true
    end
    false
  end

  def existing_coin(denomination)
    existing_coins = @coins.select{ |coin| coin.denomination == denomination }
    if existing_coins.any?
      return existing_coins.first
    end
  end

  def check_stock_of_coins
    stock_result = "Vending machine currently holds the following coins:\n\n"
    @coins.each do |coin|
      unit_string = (coin.number > 1 ? "coins" : "coin")
      stock_result += "#{coin.denomination} - #{coin.number} #{unit_string}\n"
    end
    stock_result + "\n"
  end

  def retrieve_available_coins
    @coins
  end

  def dispense_change(coins_hash)
    coins_hash.each do |key, value|
      unload_coin(key, value)
    end
  end
end
