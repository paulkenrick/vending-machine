require_relative 'coin_converter'

class UserInterface

  attr_reader :product_holder
  attr_reader :coin_holder
  attr_accessor :current_product
  attr_accessor :current_funds_received

  def initialize(product_holder, coin_holder)
    @product_holder = product_holder
    @coin_holder = coin_holder
    @current_product = nil
    @current_funds_received = 0
  end

  def ready_for_purchase
    while !has_open_transaction? do
      display_product_options
    end
  end

  def has_open_transaction?
    @current_product != nil
  end

  def display_product_options
    puts "\n**************************************\n**\n"
    puts "**  Available products:\n**\n"
    @product_holder.products.each_with_index{ |product, index| puts "**  #{index + 1}. #{product.name}  £#{sprintf('%.2f', product.price)} - [#{product.amount} left]" }
    puts "**\n**  [insert technician's key (press 'K') to see technician's options]\n**\n"
    puts "**************************************\n\n"
    print "Please choose a product (enter number and press enter): "

    user_selection = obtain_selection
    if user_selection.downcase == 'k'
      display_technicians_options
    elsif user_selection_valid?(user_selection)
      initiate_transaction_for(user_selection)
    else
      puts "\n--------------------------------------\n"
      puts "\nInvalid selection. Try again.\n\n"
    end
  end

  def obtain_selection
    user_selection = gets.chomp
  end

  def user_selection_valid?(user_selection)
    return true if user_selection.to_i > 0 && user_selection.to_i <= @product_holder.products.length
    false
  end

  def initiate_transaction_for(user_selection)
    @current_product = @product_holder.products[user_selection.to_i - 1]
    @current_funds_received = 0
    puts "\n--------------------------------------\n"
    puts "\nYou selected: #{@current_product.name}."

    collect_coins
  end

  def collect_coins
    coin_inserted = nil
    while @current_funds_received < @current_product.price do
      coin_inserted = request_coins
      if coin_inserted.downcase == 'c'
        cancel_transaction
        break
      elsif CoinConverter.valid_denomination?(coin_inserted)
        @coin_holder.load_coin(coin_inserted)
        @current_funds_received += CoinConverter.convert_coins_to_gbp({ coin_inserted => 1 })
        puts "\n--------------------------------------\n\n"
        puts "Thank you."
      else
        puts "\n--------------------------------------\n"
        puts "\nInvalid coin.  This machine accepts the following: #{CoinConverter.coins_accepted.join(', ')}."
      end
    end

    dispense_change
  end

  def request_coins
    print "There is £#{sprintf('%.2f', @current_product.price - @current_funds_received)} to pay.  Please insert coins (i.e. type the coin you wish to enter) or press 'C' to cancel: "
    coin_inserted = gets.chomp
  end

  def cancel_transaction
    @current_product = nil
    puts "\n--------------------------------------\n"
    puts "\nTransaction cancelled."
  end

  def dispense_change
    amount_to_dispense = @current_product ? @current_funds_received - @current_product.price : @current_funds_received
    coins_hash = CoinConverter.convert_gbp_to_coins(amount_to_dispense, @coin_holder)
    if coins_hash
      if !coins_hash.empty?
        @coin_holder.dispense_change(coins_hash)
        puts "Please collect your £#{sprintf('%.2f', amount_to_dispense)} change. [Coins dispensed: #{coins_hash}]"
      end
    else
      puts "Apologies, this machine does not have enough coins to provide your change."
    end

    dispense_product
  end

  def dispense_product
    if @current_product
      @product_holder.dispense(@current_product)
      puts "Please collect your #{@current_product.name} below.\n\n\n"
    end

    end_transaction
  end

  def end_transaction
    @current_product = nil
    @current_funds_received = 0
  end

  def display_technicians_options
    puts "\n--------------------------------------\n"
    puts "\n//////////////////////////////////////"
    puts "///\n"
    puts "/// Technician Actions Available:\n"
    puts "///\n"
    puts "/// 1. View product levels"
    puts "/// 2. Load products"
    puts "/// 3. Unload products"
    puts "/// 4. View coin levels"
    puts "/// 5. Load coins"
    puts "/// 6. Unload coins"
    puts "///\n/// [press 'c' to cancel]\n"
    puts "///\n"
    puts "//////////////////////////////////////\n\n"
    print "Please choose an option (enter number and press enter): "

    user_selection = obtain_selection
    case user_selection.downcase
    when 'c'
      puts "\n--------------------------------------\n\n"
      ready_for_purchase
    when '1'
      display_stock_levels
    when '2'
      load_products
    when '3'
      unload_products
    when '4'
      display_coin_levels
    when '5'
      load_coins
    when '6'
      unload_coins
    else
      puts "\n--------------------------------------\n"
      puts "\nInvalid selection. Returning to product selection.\n\n"
    end
  end

  def display_stock_levels
    puts "\n--------------------------------------\n\n"
    puts @product_holder.check_stock
  end

  def load_products
    product_details = get_product_data
    puts "\n--------------------------------------\n\n"
    if @product_holder.load_product(product_details[:name], product_details[:price], product_details[:amount])
      puts "Product loaded successfully\n\n"
    else
      puts "Sorry, product could not be loaded.\n\n"
    end
  end

  def unload_products
    product_details = get_product_data
    puts "\n--------------------------------------\n\n"
    if @product_holder.unload_product(product_details[:name], product_details[:price], product_details[:amount])
      puts "Product unloaded successfully\n\n"
    else
      puts "Sorry, product could not be unloaded.\n\n"
    end
  end

  def get_product_data
    puts "\n--------------------------------------\n\n"
    print "Please provide product name: "
    product_name = gets.chomp
    print "Please provide product price in £ (i.e. '1.40' for $1.40): "
    product_price = gets.chomp
    print "Please provide number of units: "
    product_amount = gets.chomp

    { name: product_name, price: product_price.to_f, amount: product_amount.to_i }
  end

  def display_coin_levels
    puts "\n--------------------------------------\n\n"
    puts @coin_holder.check_stock_of_coins
  end

  def load_coins
    coin_details = get_coin_data
    puts "\n--------------------------------------\n\n"
    if @coin_holder.load_coin(coin_details[:denomination], coin_details[:number])
      puts "Coin loaded successfully\n\n"
    else
      puts "Sorry, coin could not be loaded.\n\n"
    end
  end

  def unload_coins
    coin_details = get_coin_data
    puts "\n--------------------------------------\n\n"
    if @coin_holder.unload_coin(coin_details[:denomination], coin_details[:number])
      puts "Coin unloaded successfully\n\n"
    else
      puts "Sorry, coin could not be unloaded.\n\n"
    end
  end

  def get_coin_data
    puts "\n--------------------------------------\n\n"
    print "Please provide denomination (i.e. '10p' or '£1'): "
    denomination = gets.chomp
    print "Please provide number of coins: "
    number = gets.chomp

    { denomination: denomination, number: number.to_i }
  end
end
