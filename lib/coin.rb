class Coin

  attr_reader :denomination
  attr_accessor :number

  def initialize(denomination, number)
    @denomination = denomination
    @number = number
  end

end
