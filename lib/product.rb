class Product

  attr_reader :name
  attr_reader :price
  attr_accessor :amount

  def initialize(name, price, amount)
    @name = name
    @price = price
    @amount = amount
  end

end
