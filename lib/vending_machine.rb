require_relative 'product_holder'
require_relative 'coin_holder'
require_relative 'user_interface'


# ======== Initializing product holder and loading products ========
product_holder = ProductHolder.new

product_holder.load_product("Chocolate Bar", 1.40, 4)
product_holder.load_product("Packet of Crisps", 1.11, 10)
product_holder.load_product("Can of Coke", 1.00, 15)


# ======== Initializing coin holder and loading coins ========
coin_holder = CoinHolder.new

CoinConverter.coins_accepted.each do |denomination|
  coin_holder.load_coin(denomination, 10)
end


# ======== Initializing user interface ========
user_interface = UserInterface.new(product_holder, coin_holder)
user_interface.ready_for_purchase
