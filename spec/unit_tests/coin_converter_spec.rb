require 'spec_helper'
require 'coin_converter'
require 'coin_holder'

describe CoinConverter do

  describe '.valid_denomination?' do
    it 'returns true if the denomination provided is in the accepted coins constant' do
      expect(CoinConverter.valid_denomination?('10p')).to eq(true)
    end

    it 'returns false if the denomination provided is not in the accepted coins constant' do
      expect(CoinConverter.valid_denomination?('30p')).to eq(false)
    end
  end

  describe '.convert_coins_to_gbp' do
    it 'returns the total gbp value of all the coin denominations and numbers in the hash provided' do
      expect(CoinConverter.convert_coins_to_gbp({ '10p' => 2, '50p' => 1, '£1' => 1 })).to eq(1.70)
    end
  end

  describe '.convert_gbp_to_coins' do
    context 'if first choice of coins are available' do
      it 'returns a hash of the coin denominations using highest denominations first' do
        coin_10p = double(:coin)
        coin_50p = double(:coin)
        coin_1£ = double(:coin)

        allow(coin_10p).to receive(:denomination).and_return('10p')
        allow(coin_50p).to receive(:denomination).and_return('50p')
        allow(coin_1£).to receive(:denomination).and_return('£1')

        allow(coin_10p).to receive(:number).and_return(10)
        allow(coin_50p).to receive(:number).and_return(10)
        allow(coin_1£).to receive(:number).and_return(10)

        coin_holder = double(:coin_holder)
        allow(coin_holder).to receive(:retrieve_available_coins).and_return([coin_10p, coin_50p, coin_1£])

        expect(CoinConverter.convert_gbp_to_coins(1.70, coin_holder)).to eq({ '£1' => 1, '50p' => 1, '10p' => 2 })
      end
    end

    context 'if first choice of coins are unavailable' do
      it 'returns a hash of the coin denominations using next highest denominations' do
        coin_10p = double(:coin)
        coin_1£ = double(:coin)

        allow(coin_10p).to receive(:denomination).and_return('10p')
        allow(coin_1£).to receive(:denomination).and_return('£1')

        allow(coin_10p).to receive(:number).and_return(10)
        allow(coin_1£).to receive(:number).and_return(10)

        coin_holder = double(:coin_holder)
        allow(coin_holder).to receive(:retrieve_available_coins).and_return([coin_10p, coin_1£])

        expect(CoinConverter.convert_gbp_to_coins(1.70, coin_holder)).to eq({ '£1' => 1, '10p' => 7 })
      end
    end
  end

end
