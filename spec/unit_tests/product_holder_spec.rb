require 'spec_helper'
require 'product_holder'

describe ProductHolder do

  subject(:product_holder) { described_class.new }

  let(:product_class) { class_double('Product').as_stubbed_const }
  let(:product) { double('product') }

  describe '#initialize' do
    it 'initializes with a default capacity if not specified' do
      expect(subject.capacity).to eq(ProductHolder::DEFAULT_CAPACITY)
    end

    it 'initializes with a set capacity when specified' do
      product_holder = described_class.new(60)
      expect(product_holder.capacity).to eq(60)
    end
  end

  describe '#load_product' do
    before do
      allow(product_class).to receive(:new).and_return(product)
      allow(product).to receive(:name).and_return("Chocolate Bar")
      allow(product).to receive(:price).and_return(1.40)
      allow(product).to receive(:amount).and_return(1)
    end

    it 'loads a new product into the holder correctly' do
      expect(product_class).to receive(:new).with("Chocolate Bar", 1.40, 5)
      subject.load_product("Chocolate Bar", 1.40, 5)
      expect(subject.products.count).to eq(1)
    end

    it 'adds to an existing product correctly' do
      subject.products << product

      expect(product_class).to_not receive(:new)
      expect(product).to receive(:amount=).with(6)
      subject.load_product("Chocolate Bar", 1.40, 5)
      expect(subject.products.count).to eq(1)
    end
  end

  describe '#unload_product' do
    before do
      allow(product).to receive(:name).and_return("Chocolate Bar")
      allow(product).to receive(:price).and_return(1.40)
    end

    it 'unloads products from the holder correctly' do
      subject.products << product
      allow(product).to receive(:amount).and_return(5)

      expect(product).to receive(:amount=).with(4)
      subject.unload_product("Chocolate Bar", 1.40, 1)
      expect(subject.products.count).to eq(1)
    end

    it 'deletes a product from the holder completely if it the amount is reduced to 0' do
      subject.products << product
      allow(product).to receive(:amount).and_return(1, 0)
      allow(product).to receive(:amount=).with(0)

      expect(subject.products).to receive(:delete).with(product).and_call_original
      subject.unload_product("Chocolate Bar", 1.40, 1)
      expect(subject.products.count).to eq(0)
    end
  end

  describe '#check_stock' do
    it 'returns a string stating the current level of stock' do
      subject.products << product
      allow(product).to receive(:name) { "Chocolate Bar" }
      allow(product).to receive(:amount) { 1 }

      expect(subject.check_stock).to include("Chocolate Bar - 1")
    end
  end

  describe '#retrieve_available_products' do
    it 'returns array of all products current in the holder' do
      subject.products << product

      expect(subject.retrieve_available_products).to eq([product])
    end
  end

  describe '#dispense' do
    it 'reduces the amount of a product by one' do
      subject.products << product
      allow(product).to receive(:amount).and_return(5)

      expect(product).to receive(:amount=).with(4)
      subject.dispense(product)
    end

    it 'removes a product from the product holder if amount is 0' do
      subject.products << product
      allow(product).to receive(:amount).and_return(1, 0)
      expect(product).to receive(:amount=).with(0)
      subject.dispense(product)
      expect(subject.products).to eq([])
    end
  end
end
