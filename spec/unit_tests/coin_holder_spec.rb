require 'spec_helper'
require 'coin_holder'

describe CoinHolder do

  subject(:coin_holder) { described_class.new }

  let(:coin_class) { class_double('Coin').as_stubbed_const }
  let(:coin) { double(:coin) }

  describe '#initialize' do
    it 'initializes with no coins' do
      expect(subject.coins).to eq([])
    end
  end

  describe '#load_coin' do
    before do
      allow(coin_class).to receive(:new).and_return(coin)
      allow(coin).to receive(:number).and_return(1)
    end

    it 'adds a new coin into the holder correctly' do
      expect(coin_class).to receive(:new).with('10p', 1)
      subject.load_coin("10p", 1)
      expect(subject.coins.count).to eq(1)
      expect(subject.coins.first).to eq(coin)
    end

    it 'adds to an existing coin correctly' do
      subject.coins << coin
      allow(coin).to receive(:denomination).and_return('10p')

      expect(coin).to receive(:number=).with(6)
      subject.load_coin("10p", 5)
      expect(subject.coins.count).to eq(1)
      expect(subject.coins.first).to eq(coin)
    end
  end

  describe '#unload_coin' do
    before do
      allow(coin).to receive(:denomination).and_return('10p')
    end

    it 'unloads products from the holder correctly' do
      subject.coins << coin
      allow(coin).to receive(:number).and_return(5)

      expect(coin).to receive(:number=).with(4)
      subject.unload_coin('10p', 1)
      expect(subject.coins.count).to eq(1)
      expect(subject.coins.first).to eq(coin)
    end

    it 'removes a coin from the holder completely if the amount is reduced to 0' do
      subject.coins << coin
      allow(coin).to receive(:number).and_return(1, 0)
      allow(coin).to receive(:number=).with(0)

      expect(subject.coins).to receive(:delete).with(coin).and_call_original
      subject.unload_coin('10p', 1)
      expect(subject.coins.count).to eq(0)
    end
  end

  describe '#check_stock_of_coins' do
    it 'returns a string stating the coins currently in the holder' do
      subject.coins << coin
      allow(coin).to receive(:denomination).and_return("10p")
      allow(coin).to receive(:number).and_return(1)

      expect(subject.check_stock_of_coins).to include("10p - 1 coin")
    end
  end

  describe '#retrieve available coins' do
    it 'returns and array of the coins currently held in the coinholder' do
      subject.coins << coin

      expect(subject.retrieve_available_coins).to eq([coin])
    end
  end

  describe '#dispense_change' do
    it 'calls #unload on each of the coins in the hash it is passed' do
      expect(subject).to receive(:unload_coin).with('10p', 1)
      subject.dispense_change({ '10p' => 1 })
    end
  end

end
