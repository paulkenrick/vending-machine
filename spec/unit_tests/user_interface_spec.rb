require 'spec_helper'
require 'user_interface'

describe UserInterface do

   let(:subject) { described_class.new(product_holder, coin_holder) }

   let(:product_holder) { double(:product_holder) }
   let(:coin_holder) { double(:coin_holder) }
   let(:product) { double(:product) }


  describe  '#initialize' do
    it 'initializes with a product holder and coin holder attached' do
      expect(subject.product_holder).to eq(product_holder)
      expect(subject.coin_holder).to eq(coin_holder)
    end
  end

  describe '#ready_for_purchase' do
    it 'calls the #display_product_options method to display options when there is no transaction in process' do
      allow(subject).to receive(:has_open_transaction?).and_return(false, true)

      expect(subject).to receive(:display_product_options).once
      subject.ready_for_purchase
    end
  end

  describe '#has_open_trasaction' do
    it 'returns true if the user_interface has a current_product' do
      subject.current_product = product
      expect(subject.has_open_transaction?).to eq(true)
    end
  end

  describe '#display_products_options' do

    let(:orig_stdout) { $stdout }

    before do
      allow(product_holder).to receive(:products).and_return([product])
      allow(product).to receive(:name).and_return('Chocolate Bar')
      allow(product).to receive(:price).and_return(1.40)
      allow(product).to receive(:amount).and_return(5)

      allow(subject).to receive(:obtain_selection).and_return('1')
      allow(subject).to receive(:user_selection_valid?).and_return(true)
      allow(subject).to receive(:initiate_transaction_for).with('1')

      $stdout = StringIO.new
    end

    after do
      $stdout = orig_stdout
    end

    it 'displays the products in the product holder in stdout' do
      expect($stdout).to receive(:puts).with("\n**************************************\n**\n")
      expect($stdout).to receive(:puts).with("**  Available products:\n**\n")
      expect($stdout).to receive(:puts).with("**  1. Chocolate Bar  £1.40 - [5 left]")
      expect($stdout).to receive(:puts).with("**\n**  [insert technician's key (press 'K') to see technician's options]\n**\n")
      expect($stdout).to receive(:puts).with("**************************************\n\n")

      subject.display_product_options
    end

    it 'calls #obtain_selection to obtain user\'s selection' do
      expect(subject).to receive(:obtain_selection)
      subject.display_product_options
    end

    it 'calls #user_selection_valid to check if selection valid' do
      expect(subject).to receive(:user_selection_valid?)
      subject.display_product_options
    end

    it 'calls #user_selection_valid to check if selection valid' do
      expect(subject).to receive(:initiate_transaction_for)
      subject.display_product_options
    end

    context 'when user selection is \'K\' for technicians options' do
      it 'calls the #display_technicians_options method' do
        allow(subject).to receive(:obtain_selection).and_return('K')

        expect(subject).to receive(:display_technicians_options)
        subject.display_product_options
      end
    end
  end

  describe '#obtain_selection' do
    it 'waits for intput from the user' do
      expect(subject).to receive(:gets).and_return('1')
      subject.obtain_selection
    end
  end

  describe '#user_selection_valid?' do
    it 'returns true if passed a number corresponds to a selection' do
      allow(product_holder).to receive(:products).and_return([product])
      expect(subject.user_selection_valid?(1)).to eq(true)
      expect(subject.user_selection_valid?(2)).to eq(false)
    end
  end

  describe '#initiate_transaction_for' do
    before do
      allow(product_holder).to receive(:products).and_return([product])
      allow(product).to receive(:name)
    end

    it 'sets the current_product' do
      allow(subject).to receive(:collect_coins)

      subject.initiate_transaction_for(1)
      expect(subject.current_product).to eq(product)
    end

    it 'calls the #collect_coins method' do
      expect(subject).to receive(:collect_coins)
      subject.initiate_transaction_for(1)
    end
  end

  describe '#collect_coins' do
    let(:coin_converter_class) { class_double('CoinConverter').as_stubbed_const }

    before do
      subject.current_funds_received = 0
      subject.current_product = product
      allow(product).to receive(:price).and_return(1.40)
      allow(subject).to receive(:request_coins).and_return('£1', '20p', '20p')
      allow(coin_holder).to receive(:load_coin)
      allow(coin_converter_class).to receive(:valid_denomination?).with('£1').and_return(true)
      allow(coin_converter_class).to receive(:valid_denomination?).with('20p').and_return(true)
      allow(coin_converter_class).to receive(:valid_denomination?).with('30p').and_return(false)
      allow(coin_converter_class).to receive(:convert_coins_to_gbp).with({ '£1' => 1 }).and_return(1.00)
      allow(coin_converter_class).to receive(:convert_coins_to_gbp).with({ '20p' => 1 }).and_return(0.20)
      allow(subject).to receive(:dispense_change)
      allow(coin_converter_class).to receive(:coins_accepted).and_return(%w(1p 2p 5p 10p 20p 50p £1 £2))
    end

    it 'should call #request_coins to obtain a coin from the user until amount is equal to or more than product price' do
      expect(subject).to receive(:request_coins).exactly(3).times
      subject.collect_coins
    end

    context 'if coin is valid denomination' do
      it 'should call the #load_coin method to add coin to the holder' do
        expect(coin_holder).to receive(:load_coin).once.with('£1')
        expect(coin_holder).to receive(:load_coin).twice.with('20p')
        subject.collect_coins
      end
    end

    context 'if coin is not valid denomination' do
      it 'should not call the #load_coin method to add the coin to the holder' do
        allow(subject).to receive(:request_coins).and_return('30p', '£1', '20p', '20p')

        expect(coin_holder).to_not receive(:load_coin).with('30p')
        subject.collect_coins
      end
    end
  end

  describe '#request_coins' do
    it 'waits for input from user' do
      subject.current_product = product
      allow(product).to receive(:price).and_return(1.40)
      expect(subject).to receive(:gets).and_return('£1')
      subject.request_coins
    end
  end

  describe 'dispense_change' do
    before do
      subject.current_product = product
      subject.current_funds_received = 2.00

      allow(product).to receive(:price).and_return(1.40)
      coin_converter_class = class_double('CoinConverter').as_stubbed_const
      allow(coin_converter_class).to receive(:convert_gbp_to_coins).and_return({ '50p' => 1, '10p' => 1 })
      allow(subject).to receive(:dispense_product)
    end

    context 'when change is due' do
      it 'should call #dispense_change method on coin holder' do
        allow($stdout).to receive(:puts)

        expect(coin_holder).to receive(:dispense_change).with({ '50p' => 1, '10p' => 1 })
        subject.dispense_change
      end

      it 'should send message to stdout to collect change' do
        allow(coin_holder).to receive(:dispense_change).with({ '50p' => 1, '10p' => 1 })

        expect($stdout).to receive(:puts).once.with("Please collect your £0.60 change. [Coins dispensed: {\"50p\"=>1, \"10p\"=>1}]")
        subject.dispense_change
      end
    end
  end

  describe '#dispense_product' do
    before do
      subject.current_product = product
      allow(product).to receive(:name).and_return('Chocolate Bar')
    end

    it 'should call the #dispense method on the product holder' do
      allow($stdout).to receive(:puts).with("Please collect your Chocolate Bar below.\n\n\n")

      expect(subject.product_holder).to receive(:dispense).with(subject.current_product)
      subject.dispense_product
    end

    it 'should send message to stdout to collect product' do
      allow(subject.product_holder).to receive(:dispense).with(subject.current_product)

      expect($stdout).to receive(:puts).with("Please collect your Chocolate Bar below.\n\n\n")
      subject.dispense_product
    end
  end

  describe '#end_transaction' do
    it 'should return current_product to nil and current_funds_received to 0' do
      subject.current_product = product
      subject.current_funds_received = 1.40

      subject.end_transaction
      expect(subject.current_product).to eq(nil)
      expect(subject.current_funds_received).to eq(0)
    end
  end

  describe '#display_technicians_options' do
    it 'calls obtain selection to capture user input' do
      allow(subject).to receive(:obtain_selection).and_return('2')
      allow(subject).to receive(:load_products)

      expect(subject).to receive(:obtain_selection)
      subject.display_technicians_options
    end

    context 'when user input is \'c\' (cancel)' do
      it 'calls the #ready_for_purchase method to return to the product menu' do
        allow(subject).to receive(:obtain_selection).and_return('C')
        allow(subject).to receive(:ready_for_purchase)

        expect(subject).to receive(:ready_for_purchase)
        subject.display_technicians_options
      end
    end

    context 'when user selects option' do
      it 'calls the respective action' do
        allow(subject).to receive(:obtain_selection).and_return('2')
        allow(subject).to receive(:load_products)

        expect(subject).to receive(:load_products)
        subject.display_technicians_options
      end
    end
  end

  describe '#display_stock_levels' do
    it 'prints out the current stock level' do
      allow(subject.product_holder).to receive(:check_stock).and_return("Chocolate Bar - 1")
      allow($stdout).to receive(:puts).with("\n--------------------------------------\n\n")

      expect($stdout).to receive(:puts).with("Chocolate Bar - 1")
      subject.display_stock_levels
    end
  end

  describe '#load_products' do
    it 'calls the product_holder\'s #load_product method with the correct parameters' do
      allow(subject).to receive(:get_product_data).and_return({ name: "Chocolate Bar", price: 1.40, amount: 20 })

      expect(product_holder).to receive(:load_product).with("Chocolate Bar", 1.40, 20)
      subject.load_products
    end
  end

  describe '#load_products' do
    it 'calls the product_holder\'s #unload_product method with the correct parameters' do
      allow(subject).to receive(:get_product_data).and_return({ name: "Chocolate Bar", price: 1.40, amount: 20 })

      expect(product_holder).to receive(:unload_product).with("Chocolate Bar", 1.40, 20)
      subject.unload_products
    end
  end

  describe '#get_product_data' do
    it 'obtains product details from user' do
      allow(subject).to receive(:gets).and_return("Cholcolate Bar", "1.40", "10")

      expect(subject).to receive(:gets).exactly(3).times
      subject.get_product_data
    end
  end

  describe '#display_coin_levels' do
    it 'prints out the current coin level' do
      allow(subject.coin_holder).to receive(:check_stock_of_coins).and_return("10p - 5")
      allow($stdout).to receive(:puts).with("\n--------------------------------------\n\n")

      expect($stdout).to receive(:puts).with("10p - 5")
      subject.display_coin_levels
    end
  end

  describe '#load_coins' do
    it 'calls the coin_holder\'s #load_coin method with the correct parameters' do
      allow(subject).to receive(:get_coin_data).and_return({ denomination: "10p", number: 20 })

      expect(coin_holder).to receive(:load_coin).with("10p", 20)
      subject.load_coins
    end
  end

  describe '#unload_coins' do
    it 'calls the coin_holder\'s #unload_coin method with the correct parameters' do
      allow(subject).to receive(:get_coin_data).and_return({ denomination: "10p", number: 20 })

      expect(coin_holder).to receive(:unload_coin).with("10p", 20)
      subject.unload_coins
    end
  end

  describe '#get_coin_data' do
    it 'obtains coin details from user' do
      allow(subject).to receive(:gets).and_return("10p", "20")

      expect(subject).to receive(:gets).exactly(2).times
      subject.get_coin_data
    end
  end

end
